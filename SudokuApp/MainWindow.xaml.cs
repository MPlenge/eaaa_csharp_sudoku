﻿using SudokuLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace SudokuApp {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        private ISudoku sudoku;
        private Dictionary<string, SudokuGridButton> buttons;
        private ObservableCollection<string> guiBoard;
        private SudokuGridButton activeButton;
        
        //Timer Setup
        DispatcherTimer dispatcherTimer = new DispatcherTimer();
        Stopwatch stopWatch = new Stopwatch();
        string currentTime = string.Empty;

        public MainWindow() {
            InitializeComponent();
            buttons = new Dictionary<string, SudokuGridButton>();
            initGrid();
            //Timer Init
            dispatcherTimer.Tick += new EventHandler(dt_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1);
        }

        //Timer Event
        private void dt_Tick(object sender, EventArgs e)
        {
            if (stopWatch.IsRunning) {
                TimeSpan ts = stopWatch.Elapsed;
                currentTime = String.Format("{0:00}:{1:00}:{2:00}", ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                TimeBox.Content = currentTime;
            }
            if (sudoku.IsSolved()) {
                stopWatch.Stop();
            }
        }

        private void New_Game_Click(object sender, RoutedEventArgs e) {
            //Read file, replace . with 0 and split into individual sudokus
            string file = System.IO.File.ReadAllText(@"./Resources/top1465w.txt");

            string[] stringSeparators = new string[] { "\r\n" };
            string[] sudokus = file.Replace('.', '0').Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

            //Choose a sudoku
            Random r = new Random();
            string sudokuText = sudokus[r.Next() % sudokus.Length];

            newGame(sudokuText);
        }

        private void newGame(string sudokuText) {
            //Set CanChange on SudokuGridButton 
            foreach (SudokuGridButton button in buttons.Values.ToList()) {
                int num;
                int.TryParse(sudokuText.Substring(button.id, 1), out num);
                button.canChange = num == 0;
            }

            //Create sudoku
            sudoku = SudokuFactory.CreateSudoku(sudokuText);
            guiBoard = createGUIBoard(sudoku);
            SudokuGrid.DataContext = guiBoard;

            //Timer start
            stopWatch.Reset();
            stopWatch.Start();
            dispatcherTimer.Start();

            //Open cells start
            CellsBox.Content = sudoku.NumberOfOpenCells();
        }

        private void initGrid() {
            for (int y = 0; y < 9; y++) {
                for (int x = 0; x < 9; x++) {
                    Button btn = new Button();
                    btn.Tag = true;
                    btn.Name = "field_" + x + "_" + y;
                    btn.Click += setActiveButton;
                    btn.KeyDown += keyPressedHandler;
                    if (x > 5) {
                        Grid.SetColumn(btn, x + 2);
                    } else if (x > 2) {
                        Grid.SetColumn(btn, x + 1);
                    } else {
                        Grid.SetColumn(btn, x);
                    }

                    if (y > 5) {
                        Grid.SetRow(btn, y + 2);
                    } else if (y > 2) {
                        Grid.SetRow(btn, y + 1);
                    } else {
                        Grid.SetRow(btn, y);
                    }

                    Binding binding = new Binding("[" + (y * 9 + x) + "]");
                    binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                    btn.SetBinding(Button.ContentProperty, binding);

                    Binding binding2 = new Binding("ActualHeight");
                    binding2.Source = btn;
                    btn.SetBinding(Button.WidthProperty, binding2);

                    buttons[btn.Name] = new SudokuGridButton(btn, x, y);
                    SudokuGrid.Children.Add(btn);
                }

                foreach (var e in NumPadGrid.Children) {
                    Button btn = (Button)e;

                    Binding binding = new Binding("ActualHeight");
                    binding.Source = btn;
                    btn.SetBinding(Button.WidthProperty, binding);
                }
            }
        }

        private void keyPressedHandler(object sender, KeyEventArgs e) {
            if ((int)e.Key >= 34 && (int)e.Key <= 43) { //Digits
                setField((int)e.Key % 34);
            } else if ((int)e.Key >= 74 && (int)e.Key <= 83) { //Numpad
                setField((int)e.Key % 74);
            } else if (e.Key == Key.Back) { //Backspace
                setField(0);
            }
        }

        private ObservableCollection<string> createGUIBoard(ISudoku sudoku) {
            ObservableCollection<string> guiBoard = new ObservableCollection<string>();

            for (int y = 0; y < 9; y++) {
                for (int x = 0; x < 9; x++) {
                    guiBoard.Add((sudoku[y, x] != 0 ? sudoku[y, x].ToString() : ""));
                }
            }
            return guiBoard;
        }

        private void Solve_Click(object sender, RoutedEventArgs e) {
            if (sudoku != null) {
                ISudoku solved = sudoku.Solve();
                SudokuGrid.DataContext = createGUIBoard(solved);
            
                //Timer stop
                if (stopWatch.IsRunning) {
                    stopWatch.Stop();
                }
            }
        }

        private void setActiveButton(object sender, RoutedEventArgs e) {
            SudokuGridButton clicked = buttons[((Button)sender).Name];

            if (activeButton != null) {
                activeButton.button.Background = Brushes.LightGray;
            }
            activeButton = clicked;
            clicked.button.Background = Brushes.LightBlue;
        }

        private void numpad_Click(object sender, RoutedEventArgs e) {
            Button clicked = (Button)sender;

            int num;
            int.TryParse((string)clicked.Content, out num);

            setField(num);
        }

        private void setField(int i) {
            if (activeButton != null && sudoku != null) {
                int oldVal = sudoku[activeButton.y, activeButton.x];
                if (activeButton.canChange) {
                    sudoku[activeButton.y, activeButton.x] = 0;
                    List<byte> possible = sudoku.PossibleNumbers(activeButton.y, activeButton.x);

                    if (possible != null && possible.Count > 0) {
                        if (i == 0) {
                            guiBoard[activeButton.id] = "";
                            sudoku.SetNumberAt(activeButton.y, activeButton.x, Convert.ToByte(i));
                        } else if (possible.Contains(Convert.ToByte(i))) {
                            guiBoard[activeButton.id] = $"{i}";
                            sudoku.SetNumberAt(activeButton.y, activeButton.x, Convert.ToByte(i));

                        } else {
                            sudoku[activeButton.y, activeButton.x] = Convert.ToByte(oldVal);
                        }

                    } else {
                        sudoku[activeButton.y, activeButton.x] = Convert.ToByte(oldVal);
                    }
                }
            }
        }

        private void Erase_Click(object sender, RoutedEventArgs e) {
            if (activeButton != null && sudoku != null) {
                setField(0);
            }
        }

        private void Hint_Click(object sender, RoutedEventArgs e) {
            if (activeButton != null && sudoku != null) {
                setField(0);
                ISudoku solved = sudoku.Solve();
                setField(solved[activeButton.y, activeButton.x]);
                activeButton.canChange = false;
            }
        }

        private void Save_Game_Click(object sender, RoutedEventArgs e) {
            if (sudoku != null) {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"./saveGame.txt", false)) {
                    string s = "";
                    for (int y = 0; y < 9; y++) {
                        for (int x = 0; x < 9; x++) {
                            s += sudoku[y, x];
                        }
                    }

                    file.WriteLine(s);
                }
            }
        }

        private void Load_Game_Click(object sender, RoutedEventArgs e) {
            try {
                string file = System.IO.File.ReadAllText(@"./saveGame.txt");
                string sudokuText = file.Replace("\r\n", "");
                newGame(sudokuText);
            } catch (Exception) {
            }
        }
    }
}
