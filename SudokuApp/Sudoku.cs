﻿using SudokuLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuApp {
    class Sudoku : ISudoku {
        private byte[] data = new byte[81];

        //Malthe
        public byte this[int y, int x] { get => data[(y * 9) + x]; set => data[(y * 9) + x] = value; }

        //Henrik
        //Make a copy of the Sudoku
        public ISudoku Clone() {
            throw new NotImplementedException();
        }

        //Henrik
        public byte GetNumberAt(int i, int j) {
            throw new NotImplementedException();
        }

        //Henrik
        //Conflicts are dublicate numbers in Rows, Columns, and Blocks.
        public bool HasConflicts() {
            throw new NotImplementedException();
        }

        //Malthe
        public bool IsSolvable() {
            throw new NotImplementedException();
        }

        //Malthe
        public bool IsSolved() {
            throw new NotImplementedException();
        }

        //Malthe
        //81 - NumberOfOpenCells
        public int NumberOfFilledCells() {
            throw new NotImplementedException();
        }

        //Malthe
        //Number of cells not containing numbers 1-9 yet
        public int NumberOfOpenCells() {
            throw new NotImplementedException();
        }

        //Malthe
        // Each unfilled position has a number of possible numbers this returns the x,y cords of the one with the least amount
        public int OptimalPosition(out int I, out int J) {
            throw new NotImplementedException();
        }

        //Henrik
        // Get list of Non-conflicting numbers at position (i,j)
        public List<byte> PossibleNumbers(int i, int j) {
            throw new NotImplementedException();
        }

        //Henrik
        public void SetNumberAt(int i, int j, byte number) {
            throw new NotImplementedException();
        }

        //Malthe
        public ISudoku Solve(SolverOutput log = null) {
            throw new NotImplementedException();
        }
    }
}
