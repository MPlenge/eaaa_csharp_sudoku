﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SudokuApp
{
    class SudokuGridButton
    {
        public Button button { get; set; }
        public int id { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public bool canChange { get; set; }

        public SudokuGridButton(Button button, int x, int y, bool canChange=false) {
            this.button = button;
            this.x = x;
            this.y = y;
            this.id = (y * 9 + x);
            this.canChange = canChange;
        }
    }
}
